import be.kdg.model.Student;

import java.util.ArrayList;
import java.util.List;

public class DemoStudent {
    public static void main(String[] args) {
        List<Student> studentList = new ArrayList<>();

        /*
        studentList.add(new Student(9999, "Charlotte Vermeulen", LocalDate.of(2000, 1, 24), "Antwerpen"));
        studentList.add(new Student(666, "Donald Trump", LocalDate.of(1946, 6, 14), "Washington"));
        studentList.add(new Student(12345, "Sam Gooris", LocalDate.of(1973, 4, 10)));
        studentList.add(new Student(12345, "Koen Schram", LocalDate.of(1967, 5, 15), "Merksem"));
        */
        System.out.println("studentList volgens studnr:");

        System.out.println("\nstudentList volgens leeftijd:");

        //Overzetten naar HashSet:
        System.out.println("\nHashSet:");

        //Overzetten naar TreeSet:
        System.out.println("\nTreeSet:");

        //Overzetten naar TreeMap:
        System.out.println("\nTreeMap:");

    }
}
